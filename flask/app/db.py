import sqlite3
import os

db_path = "/home/ashrude/projects/wcc_demo/test.db"

if os.path.exists(db_path):
    os.remove(db_path)
with open(db_path, 'w') as file:
    pass


db = sqlite3.connect(db_path)
cur = db.cursor()
querys = [
    """
    CREATE TABLE users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username VARCHAR(255) UNIQUE,
        password TEXT,
        full_name VARCHAR(255),
        pronouns VARCHAR(255),
        email VARCHAR(255),
        pfp VARCHAR(255),

        share_name BOOLEAN,
        share_email BOOLEAN,

        toc BIGINT
    )
    """,
    """
    CREATE TABLE posts (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        uid INTEGER,
        text_content TEXT,

        toc BIGINT,

        FOREIGN KEY (uid) REFERENCES users(id)

    )
    """,
    """
    CREATE TABLE comments (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        uid INTEGER,
        pid INTEGER,
        content TEXT,

        toc BIGINT,

        FOREIGN KEY (uid) REFERENCES users(id),
        FOREIGN KEY (pid) REFERENCES posts(id)
    )
    """,
    """
    CREATE TABLE followers (
        follower INTEGER NOT NULL,
        followee INTEGER NOT NULL,

        FOREIGN KEY (follower) REFERENCES users(id),
        FOREIGN KEY (followee) REFERENCES users(id)
    )
    """,
    """
    CREATE TABLE post_likes (
        uid INTEGER NOT NULL,
        pid INTEGER NOT NULL,

        FOREIGN KEY (uid) REFERENCES users(id),
        FOREIGN KEY (pid) REFERENCES posts(id)
    )
    """,
    """
    CREATE TABLE comment_likes (
        uid INTEGER NOT NULL,
        cid INTEGER NOT NULL,

        FOREIGN KEY (uid) REFERENCES users(id),
        FOREIGN KEY (cid) REFERENCES comments(id)
    )
    """
]

examples = [
    """
    INSERT INTO users (username, password, full_name)
    VALUES ('example', 'dwafgergytjfthwefw', 'Ashley Rudelsheim')
    """,
    """
    INSERT INTO users (username, password, full_name)
    VALUES ('example2', '1f950d35206f8c941759d0e7b6039919ad9ad64a443a86b995d03342596f92da:170a76f53c3841c689591867ac2b9a79', 'Ashley Rudelsheim')
    """,
    """
    INSERT INTO posts (uid, text_content)
    VALUES (1, 'Hello World!')
    """,
    """
    INSERT INTO posts (uid, text_content)
    VALUES (1, 'Hello World! 2')
    """,
    """
    INSERT INTO comments (uid, pid, content)
    VALUES (1, 1, 'Hello World!')
    """
]


for query in querys:
    cur.execute(query)
for example in examples:
    cur.execute(example)

db.commit()
