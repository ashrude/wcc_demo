from flask import Flask
from flask_httpauth import HTTPBasicAuth

app = Flask(__name__)
auth=HTTPBasicAuth()

@auth.verify_password
def authenticate(username, password):
    print(username)
    print(password)
    return False

@app.route('/')
@auth.login_required
def root():
    return "authed"

app.run(port=4000)