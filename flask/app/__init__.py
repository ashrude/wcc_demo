from flask import Flask, request, Response, send_file
from werkzeug.utils import secure_filename
from flask_httpauth import HTTPBasicAuth
import sqlite3
import hashlib
import json
import time
import uuid
import os

app = Flask(__name__)
auth = HTTPBasicAuth()


db_path = "/home/ashrude/projects/wcc_demo/test.db"

## authentication setup

def passwdhash(password):
        salt = uuid.uuid4().hex
        return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ":" + salt
def passwdcheck (hashed, password):
        hpassword, salt = hashed.split(':')
        return hpassword == hashlib.sha256(salt.encode() + password.encode()).hexdigest()

@auth.verify_password
def authenticate(username, password):
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if not username or not password:
        return False

    cur.execute("""
    SELECT password FROM users WHERE username=:username
    """, {"username": username})
    hashed = cur.fetchall()
    if hashed == [] or hashed == [(None,)]:
        return False
    else:
        return passwdcheck(hashed[0][0], password)



# user facing

mimetypes = {
    "html": "text/html",
    "js": "application/javascript",
    "css": "text/css",
    "json": "application/json",
    "img": "image/x-icon",
    "jpg": "image/jpeg",
    "svg": "image/svg+xml"
}

@app.route('/')
@auth.login_required
def srv_return_homepage():
    return auth.current_user()

@app.route('/static/<filetype>/<filename>')
@auth.login_required
def flask_static(filetype, filename):
    if filetype == "img" or filetype == "jpg":
        return send_file(f"static/{filetype}/{filename}")
    try:
        mimetype = mimetypes[filetype]
        file = open(f"app/static/{filetype}/{filename}")
        data = file.read()
        file.close()
    except:
        return Response(status=404)
    
    return Response(data, mimetype=mimetype)




# API


## GET

@app.route('/api/v1/users')
def api_users():
    if request.args['scope']:
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        if request.args['scope'] == 'search' and request.args['query']:
            #TODO implement search
            cur.execute("""
            SELECT id, username, pfp FROM users WHERE username = :query
            """, {'query': request.args['query']})

            curout = cur.fetchall()

        if request.args['scope'] == 'all':
            cur.execute("""
            SELECT id, username, pfp FROM users
            """)
            curout = cur.fetchall()

        out = []
        for user in curout:
            out.append({
            "id": user[0],
            "username": user[1],
            "pfp": user[2]
        })

        return json.dumps(out)

@app.route('/api/v1/users/<id>')
@auth.login_required
def api_user(id):
    if id:
        db = sqlite3.connect(db_path)
        cur = db.cursor()

        if id == 'self':
            cur.execute("""
            SELECT id FROM users WHERE username=:username
            """, {"username": auth.current_user()})
            id = cur.fetchall()[0][0]

        cur.execute("""
        SELECT id, username, full_name, pronouns, email, pfp, toc, share_name, share_email FROM users WHERE id=:id
        """, {'id': id})
        curout = cur.fetchall()[0]


        out = {
            "id": curout[0],
            "username": curout[1],
            #name
            "pronouns": curout[3],
            #email
            "pfp": curout[5],
            "toc": curout[6]
        }
        if curout[8] == 1:
            print("email")
            out["email"] = curout[4]
        if curout[7] == 1:
            print("name")
            out["name"] = curout[2]


        return json.dumps(out)

@app.route('/api/v1/posts')
@auth.login_required
def api_posts():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if 'uid' in request.args:
        cur.execute("""
        SELECT id, uid, text_content, toc FROM posts WHERE uid=:uid
        """, {"uid": request.args['uid']})
        curout = cur.fetchall()
    else:
        cur.execute("""
        SELECT id, uid, text_content, toc FROM posts
        """)
        curout = cur.fetchall()
    
    out = []
    for post in curout:

        cur.execute("""
        SELECT COUNT(pid) FROM post_likes WHERE pid=:pid
        """, {'pid': post[0]})
        likes = cur.fetchall()[0][0]

        cur.execute("""
        SELECT id FROM users WHERE username=:username
        """, {"username": auth.current_user()})
        id = cur.fetchall()[0][0]

        cur.execute("""
        SELECT * FROM post_likes WHERE uid=:uid AND pid=:pid
        """, {"uid": id, "pid": post[0]})
        liked_curout = cur.fetchall()

        if liked_curout == []:
            liked = False
        else:
            liked = True

        out.append({
            "pid": post[0],
            "uid": post[1],
            "text_content": post[2],
            "toc": post[3],
            "likes": likes,
            "liked": liked
        })
        #TODO count likes
        #TODO implement image sharing

    return json.dumps(out)

@app.route('/api/v1/comments')
@auth.login_required
def api_comments():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if request.args['pid']:
        cur.execute("""
        SELECT id, uid, pid, toc, content FROM comments WHERE pid=:pid
        """, {"pid": request.args['pid']})
        curout = cur.fetchall()

    out = []

    for comment in curout:

        cur.execute("""
        SELECT COUNT(cid) FROM comment_likes WHERE cid=:cid
        """, {'cid': comment[0]})
        likes = cur.fetchall()[0][0]

        cur.execute("""
        SELECT id FROM users WHERE username=:username
        """, {"username": auth.current_user()})
        id = cur.fetchall()[0][0]

        cur.execute("""
        SELECT * FROM comment_likes WHERE uid=:uid AND cid=:cid
        """, {"uid": id, "cid": comment[0]})
        liked_curout = cur.fetchall()

        if liked_curout == []:
            liked = False
        else:
            liked = True


        out.append({
            "id": comment[0],
            "uid": comment[1],
            "pid": comment[2],
            "toc": comment[3],
            "content": comment[4],
            "likes": likes,
            "liked": liked
        })

    return json.dumps(out)


## POST, DELETE

@app.route('/api/v1/action/account', methods=['POST', 'DELETE'])
@auth.login_required
@auth.login_required
def api_account():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    #TODO implement adding accounts

    if request.method == 'POST':
        req = list(request.json.keys())

        updatable_values = ['username', 'full_name', 'pronouns', 'email', 'email', 'pfp', 'share_name', 'share_email']

        set_values = ""
        values = {}

        for key in req:
            if key in updatable_values:
                values[key] = request.json[key]

                set_values = f"{set_values}{key} = :{key}, "

        cur.execute("""
        SELECT id FROM users WHERE username=:username
        """, {"username": auth.current_user()})
        uid = cur.fetchall()[0][0]
        values['uid'] = uid


        query = f"""
        UPDATE users SET {set_values[0:-2]} WHERE id=:uid
        """

        cur.execute(query, values)
        db.commit()



        return Response('1', mimetype="application/json")

    if request.method == 'DELETE' and request.json['id']:
        cur.execute("""
        DELETE FROM users WHERE id=:id
        """, {"id": request.json['id']})
        db.commit()
        return Response('1', mimetype="application/json")

@app.route('/api/v1/action/createaccount', methods=['POST'])
def api_create_account():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    hashed = passwdhash(request.json['password'])

    try:
        cur.execute("""
        INSERT INTO users (username, password)
        VALUES (:username, :password)
        """, {"username": request.json['username'], "password": hashed})
    except:
        return Response('database error, username may already exist')
    db.commit()

    return Response('1', mimetype="application/json")


@app.route('/api/v1/action/post', methods=['POST', 'DELETE'])
@auth.login_required
def api_post():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if request.method == 'POST':
        cur.execute("""
        INSERT INTO posts (uid, text_content)
        VALUES (:uid, :text_content)
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")
    if request.method == 'DELETE':
        cur.execute("""
        DELETE FROM posts WHERE id=:id
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")

@app.route('/api/v1/action/comment', methods=['POST', 'DELETE'])
@auth.login_required
def api_comment():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if request.method == 'POST':
        cur.execute("""
        INSERT INTO comments (uid, pid, content)
        VALUES (:uid, :pid, :content)
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")
    if request.method == 'DELETE':
        cur.execute("""
        DELETE FROM comments WHERE id=:id
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")

@app.route('/api/v1/action/like_post', methods=['POST', 'DELETE'])
@auth.login_required
def api_like_post():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if request.method == 'POST':
        cur.execute("""
        INSERT INTO post_likes (uid, pid)
        VALUES (:uid, :pid)
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")

    if request.method == 'DELETE':
        cur.execute("""
        DELETE FROM post_likes WHERE uid=:uid AND pid=:pid
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")

@app.route('/api/v1/action/like_comment', methods=['POST', 'DELETE'])
@auth.login_required
def api_like_comment():
    db = sqlite3.connect(db_path)
    cur = db.cursor()

    if request.method == 'POST':
        cur.execute("""
        INSERT INTO comment_likes (uid, cid)
        VALUES (:uid, :cid)
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")

    if request.method == 'DELETE':
        cur.execute("""
        DELETE FROM comment_likes WHERE uid=:uid AND cid=:cid
        """, request.json)
        db.commit()
        return Response('1', mimetype="application/json")