function remove_children (parent) {
    while (parent.lastChild) {
        parent.removeChild(parent.lastChild);
    }
}

function push () {
    //push comments
    let comments = document.getElementsByClassName("comment_push");

    for (comment of comments) {
        let pid = comment.parentNode.parentNode.id.replace("post_", "");
        let content = comment.value;

        if (content) {
            fetch(`${window.origin}/api/v1/action/comment`, {
                method: 'POST',
                body: JSON.stringify({"pid": pid, "uid": 2, "content": content}),
                headers: {'Content-type': 'application/json; charset=UTF-8'}
            })
            .then(res => refresh_posts())

        }
    }

    let post_form = document.getElementById("post_form");
    if (post_form.value) {
        fetch(`${window.origin}/api/v1/action/post`, {
            method: 'POST',
            body: JSON.stringify({"uid": window.user.id, "text_content": post_form.value}),
            headers: {'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(res => refresh_posts())
    }


}

function refresh_posts () {
    remove_children(document.getElementById("posts"))
    populate_posts(document.getElementById("posts"))
}


function populate_userinfo (elm, id) {

    fetch(`${window.origin}/api/v1/users/${id}`)
    .then(data => data.json())
    .then(json => {
        let name = document.createElement("h3");
        name.innerHTML = json.username;
        elm.appendChild(name);
    })

}

async function like_post (id) {

    let res = await fetch(`${window.origin}/api/v1/users/self`);
    window.user = await res.json();
    
    if (document.getElementById(`like_button_${id}`).innerHTML === "like") {
        fetch(`${window.origin}/api/v1/action/like_post`, {
            method: 'POST',
            body: JSON.stringify({"pid": id, "uid": window.user.id}),
            headers: {'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(res => refresh_posts())
    } else if (document.getElementById(`like_button_${id}`).innerHTML === "unlike") {
        fetch(`${window.origin}/api/v1/action/like_post`, {
            method: 'DELETE',
            body: JSON.stringify({"pid": id, "uid": window.user.id}),
            headers: {'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(res => refresh_posts())
    }

}
async function like_comment (id) {

    let res = await fetch(`${window.origin}/api/v1/users/self`);
    window.user = await res.json();
    
    if (document.getElementById(`comment_like_button_${id}`).innerHTML === "like") {
        fetch(`${window.origin}/api/v1/action/like_comment`, {
            method: 'POST',
            body: JSON.stringify({"cid": id, "uid": window.user.id}),
            headers: {'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(res => refresh_posts())
    } else if (document.getElementById(`comment_like_button_${id}`).innerHTML === "unlike") {
        fetch(`${window.origin}/api/v1/action/like_comment`, {
            method: 'DELETE',
            body: JSON.stringify({"cid": id, "uid": window.user.id}),
            headers: {'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(res => refresh_posts())
    }

}

function populate_comments (container, pid) {
    fetch(`${window.origin}/api/v1/comments?pid=${pid}`)
    .then(data => data.json())
    .then(json => {
        for (comment of json) {
            let comment_elm = document.createElement("div");

            let user_info = document.createElement("div");
            let content = document.createElement("p");
            let like_button = document.createElement("div");
            let likes = document.createElement("p")

            populate_userinfo(user_info, comment.uid);
            content.innerHTML = comment.content;
            likes.innerHTML = "likes: " + comment.likes;
            
            like_button.classList.add("like_button");

            
            if (comment.liked) {
                like_button.innerHTML = "unlike";
            } else {
                like_button.innerHTML = "like";
            }
            like_button.setAttribute("onclick", `like_comment(${comment.id})`);
            like_button.id = `comment_like_button_${comment.id}`;

            comment_elm.appendChild(user_info);
            comment_elm.appendChild(content);
            comment_elm.appendChild(like_button);
            comment_elm.appendChild(likes);

            container.appendChild(comment_elm);
        }

        let text_box = document.createElement("input");
        text_box.setAttribute("type", "text");
        text_box.id = `comment_form_${pid}`
        let comment_button = document.createElement("div");
        comment_button.innerHTML = "comment";
        comment_button.onclick = push;
        text_box.classList.add("comment_push");

        container.appendChild(text_box);
        container.appendChild(comment_button);
    })
}

function populate_posts (container, user=null) {


    fetch(`${window.origin}/api/v1/posts?sort=new`)
    .then(data => data.json())
    .then(json => {
        for (post of json) {
            
            let post_elm = document.createElement("div");
            post_elm.classList.add("post")
            post_elm.id = `post_${post.pid}`

            let user_info = document.createElement("div")
            let text_content = document.createElement("p");
            
            text_content.innerHTML = post.text_content;
            populate_userinfo(user_info, post.uid);

            post_elm.appendChild(user_info);
            post_elm.appendChild(text_content);


            let like_button = document.createElement("div");
            like_button.classList.add("like_button");
            
            if (post.liked) {
                like_button.innerHTML = "unlike";
            } else {
                like_button.innerHTML = "like";
            }

            like_button.id = `like_button_${post.pid}`
            like_button.setAttribute("onclick", `like_post(${post.pid})`)

            let likes = document.createElement("p");
            likes.innerHTML = "likes: " + post.likes;
            likes.classList.add("likes")

            let comments = document.createElement("div");
            comments.classList.add("comments");
            populate_comments(comments, post.pid);


            post_elm.appendChild(like_button);
            post_elm.appendChild(likes);
            post_elm.appendChild(comments);
            container.appendChild(post_elm);
        }

        let new_post = document.createElement("div");
        let text_box = document.createElement("input");
        let post_button = document.createElement("div");

        text_box.setAttribute("type", "text");
        text_box.id = `post_form`;
        post_button.innerHTML = "post"
        post_button.onclick = push

        new_post.appendChild(text_box);
        new_post.appendChild(post_button);
        container.appendChild(new_post);
    })
}

function create_menu () {
    conf = [
        {}
    ]


}


window.addEventListener('load', () => {
    fetch(`${window.origin}/api/v1/users/self`)
    .then(data => data.json())
    .then(json => window.user = json)
    

    let container = document.createElement("div");
    container.id = "posts";
    populate_posts(container);
    document.body.appendChild(container);

}, false);
